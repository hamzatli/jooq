package com.example.jooqdemo.controller;

import com.example.jooqdemo.dto.ContactDto;
import com.example.jooqdemo.dto.StudentCreateRequestDto;
import com.example.jooqdemo.dto.StudentDetails;
import com.example.jooqdemo.dto.StudentInfoDto;
import com.example.jooqdemo.dto.StudentResponseDto;
import com.example.jooqdemo.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.jooqdemo.db.Tables.CONTACTS;
import static com.example.jooqdemo.db.Tables.STUDENTS;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/jooq/students")
public class StudentController {

    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    @GetMapping
    public List<StudentResponseDto> getAll() {
        return studentRepository.getAll().stream()
                .map(record -> StudentResponseDto.builder()
                        .id(record.get(STUDENTS.ID))
                        .name(record.get(STUDENTS.NAME))
                        .lastName(record.get(STUDENTS.LAST_NAME))
                        .fatherName(record.get(STUDENTS.FATHER_NAME))
                        .about(record.get(STUDENTS.ABOUT))
                        .description(record.get(STUDENTS.DESCRIPTION))
                        .shortDesc(record.get(STUDENTS.SHORT_DESC))
                        .contact(ContactDto.builder()
                                .id(record.get(CONTACTS.ID))
                                .branch(record.get(CONTACTS.BRANCH))
                                .email(record.get(CONTACTS.EMAIL))
                                .fb(record.get(CONTACTS.FB))
                                .linkedin(record.get(CONTACTS.LINKEDIN))
                                .instagram(record.get(CONTACTS.INSTAGRAM))
                                .build())
                        .build())
                .collect(Collectors.toList());
    }

    @GetMapping("/info")
    public List<StudentInfoDto> getStudentsInfo() {
        return studentRepository.getStudentsInfo().stream()
                .map(record -> StudentInfoDto.builder()
                        .id(record.get(STUDENTS.ID))
                        .name(record.get(STUDENTS.NAME))
                        .lastName(record.get(STUDENTS.LAST_NAME))
                        .contactId(record.get(CONTACTS.ID))
                        .contactBranch(record.get(CONTACTS.BRANCH))
                        .contactEmail(record.get(CONTACTS.EMAIL))
                        .build()
                ).collect(Collectors.toList());
    }

    @GetMapping("/details")
    public List<StudentDetails> getStudentsDetails() {
        return studentRepository.getStudentsDetails().stream()
                .map(record -> modelMapper.map(record, StudentDetails.class)
                ).collect(Collectors.toList());
    }

    @PostMapping
    public Long createStudent(@RequestBody StudentCreateRequestDto requestDto){
        return studentRepository.create(requestDto);
    }

    @PostMapping("/batch")
    public List<Long> batchInsert(@RequestBody List<StudentCreateRequestDto> requestDto) {
       return studentRepository.batchInsert(requestDto);
    }
}