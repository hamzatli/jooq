package com.example.jooqdemo.dto;

import lombok.Data;

@Data
public class ContactCreateDto {
    private String branch;
    private String email;
    private String fb;
    private String linkedin;
    private String instagram;
}
